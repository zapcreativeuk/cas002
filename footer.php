        <div data-component-name="footer" data-component-container="true" class="nv-footer-component nv-component aem-GridColumn aem-GridColumn--default--12">
            <footer class="nr-footer nr-row disclaimer-is-hidden" role="contentinfo">
                <div class="nr-footer__wrapper">
                    <div class="nr-footer__content">
                        <div class="nr-row">
                            <aside class="nr-footer__aside nr-col-extra-large-9 nr-col-large-8 nr-col-medium-15 nr-col-small-8 nr-col-extra-small-16">
                                <p class="nr-footer__inscription">Castrol Limited</p>
                                <p class="nr-footer__rights">Copyright © 1999-2020</p>
                            </aside>
                            <ul class="nr-footer__nav nr-clearfix nr-col-large-24 nr-col-extra-large-23 nr-col-medium-17 nr-col-small-8 nr-col-extra-small-16 nr-row">
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/castrol-story/contact-us.html" target="_self">
                                        Contact Us
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/legal-notice.html" target="_self">
                                        Legal Notice
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/privacy-statement.html" target="_self">
                                        Privacy Statement
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/technology-and-innovation.html" target="_self">
                                        Technology &amp; Innovation
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/castrol-story.html" target="_self">
                                        Castrol Story
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/castrol-story/hse-policy.html" target="_self">
                                        HSE Policy
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/site-map.html" target="_self">
                                        Sitemap
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/standard-conditions-of-sale.html" target="_self">
                                        STANDARD CONDITIONS OF SALE
                                    </a>
                                </li>
                            
                                <li class="nr-col-large-10 nr-col-medium-14 nr-col-small-7 nr-col-extra-small-16">
                                    <a class="nr-footer__nav-item nr-footer__links" href="/en_gb/united-kingdom/home/cookie-preferences.html" target="_self">
                                        COOKIE PREFERENCES
                                    </a>
                                </li>
                            </ul>
                            <p class="nr-footer__social-title">Connect with us on:</p>
                            <ul class="nr-footer__social nr-clearfix">
                                <li class="nr-footer__social-item">
                                    <a class="nr-footer__link nr-footer__links" rel="noopener" href="https://facebook.com/castrol" target="_blank">
                                        <img src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/social-icons/fb.svg" width="20px" height="20px" class="nr-footer__icon" alt="Facebook">
                                    </a>
                                </li>
                            
                                <li class="nr-footer__social-item">
                                    <a class="nr-footer__link nr-footer__links" rel="noopener" href="https://www.linkedin.com/company/castrol" target="_blank">
                                        <img src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/social-icons/linkedin.svg" width="20px" height="20px" class="nr-footer__icon" alt="Linkedin">
                                    </a>
                                </li>
                            
                                <li class="nr-footer__social-item">
                                    <a class="nr-footer__link nr-footer__links" rel="noopener" href="https://twitter.com/castrol" target="_blank">
                                        <img src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/social-icons/twitter.svg" width="20px" height="20px" class="nr-footer__icon" alt="Twitter">
                                    </a>
                                </li>
                            
                                <li class="nr-footer__social-item">
                                    <a class="nr-footer__link nr-footer__links" rel="noopener" href="https://www.instagram.com/castrol_global" target="_blank">
                                        <img src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/social-icons/instagram.svg" width="20px" height="20px" class="nr-footer__icon" alt="Instagram">
                                    </a>
                                </li>
                            </ul>
                            <div class="nr-footer__disclaimer">
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>