<?php

    function cas002_resources() {
        // Scripts
        wp_enqueue_script( 'cas002_vendors', get_template_directory_uri() . '/dist/js/vendors.bundle.js', array(), false, true  );
        wp_enqueue_script( 'cas002_scripts', get_template_directory_uri() . '/dist/js/scripts.bundle.js', array(), false, true  );
        wp_enqueue_script( 'cas002_main', get_template_directory_uri() . '/dist/js/main.bundle.js', array(), false, true  );

        // Stylesheets
        wp_enqueue_style( 'cas002_fontawesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css', NULL, microtime() );
        wp_enqueue_style( 'cas002_main_styles', get_template_directory_uri() . '/dist/css/main.css', NULL, microtime() );
        wp_enqueue_style( 'cas002_styles', get_stylesheet_uri(), NULL, microtime() );
    }

    add_action( 'wp_enqueue_scripts', 'cas002_resources' );   

    function cas002_features() {
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
    }

    add_action( 'after_setup_theme', 'cas002_features' );
    

    /**
     *  Check if ACF is activated
     */
    if ( function_exists( 'acf_register_block_type' ) ) {
        /**
         * Adding specific ACF action
         */
        add_action( 'acf/init', 'register_acf_block_types' );
    }

    function register_acf_block_types() {

    /**
     * Content Components
     */

        /**
         * Hero Component
         */
        acf_register_block_type(
            array(
                'name'              => 'hero',
                'title'             => __( 'Hero' ),
                'description'       => __( 'Hero component' ),
                'render_template'   => 'template-parts/component-hero.php',
                'icon'              => 'format-gallery',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'hero',
                                        'banner'
                                    )
            )
        );

        /**
         * Text Component
         */
        acf_register_block_type(
            array(
                'name'              => 'text',
                'title'             => __( 'Text' ),
                'description'       => __( 'Text block' ),
                'render_template'   => 'template-parts/component-text.php',
                'icon'              => 'format-aside',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'text'
                                    )
            )
        );

        /**
         * Three Columns + Images Component
         */
        acf_register_block_type(
            array(
                'name'              => 'threeecolumn component',
                'title'             => __( 'Three Column Component' ),
                'description'       => __( 'three column component block' ),
                'render_template'   => 'template-parts/component-columns.php',
                'icon'              => 'columns',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'columns'
                                    )
            )
        );

        /**
         * Divider
         */
        acf_register_block_type(
            array(
                'name'              => 'divider',
                'title'             => __( 'Divider' ),
                'description'       => __( 'Divider component' ),
                'render_template'   => 'template-parts/divider.php',
                'icon'              => 'minus',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'divider'
                                    )
            )
        );

        /**
         * Benefits Components
         */
        acf_register_block_type(
            array(
                'name'              => 'benefits',
                'title'             => __( 'Benefits' ),
                'description'       => __( 'Benefits component' ),
                'render_template'   => 'template-parts/component-benefits.php',
                'icon'              => 'welcome-learn-more',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'benefits'
                                    )
            )
        );

        /**
         * Features
         */
        acf_register_block_type(
            array(
                'name'              => 'features',
                'title'             => __( 'Features' ),
                'description'       => __( 'Features component' ),
                'render_template'   => 'template-parts/component-features.php',
                'icon'              => 'align-wide',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'features'
                                    )
            )
        );

        /**
         * Helping
         */
        acf_register_block_type(
            array(
                'name'              => 'helping',
                'title'             => __( 'Helping' ),
                'description'       => __( 'Helping component' ),
                'render_template'   => 'template-parts/component-helping.php',
                'icon'              => 'editor-help',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'helping'
                                    )
            )
        );

        /**
         * Call 2 Action
         */
        acf_register_block_type(
            array(
                'name'              => 'c2a',
                'title'             => __( 'Call 2 Action' ),
                'description'       => __( 'Call 2 Action component' ),
                'render_template'   => 'template-parts/component-cta.php',
                'icon'              => 'email-alt2',
                'mode'              => 'edit',
                'keywords'          => array(
                                        'c2a',
                                        'mail'
                                    )
            )
        );
    }