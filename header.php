<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <?php wp_head(); ?>
</head>
<body class="nr-navigation--presence">

    <header class="nr-header nr-header__home">
        <div class="nr-header__inner-wrap nr-clearfix">

                
            <section class="nr-navigation__header nr-header__logo">
                <a href="/en_gb/united-kingdom/home.html" class="nr-navigation__logo">
                    <img class="nr-navigation__logo-img" width="100%" height="100%" src="https://www.castrol.com/apps/settings/wcm/designs/refresh/castrol/images/castrol.svg" alt="Castrol Logo">
                </a>
            </section>
                

            <div class="nr-header__container">

                <ul class="nr-header__links">
                    <li class="nr-header__link-wrap">
                        <a href="/en_gb/united-kingdom/home/site-tunnel.html" class="nr-header__link">
                            
                            <i class="nr-header__icon">
                                <img class="nr-icon-arrow nr-header__icon" width="100%" height="100%" src="<?php echo get_theme_file_uri(); ?>/inc/img/CAS002_Complete_CMS-icon-arrow.svg" alt="" style="height: 18px">
                            </i>
                            
                            
                            <span class="nr-header__link-text">Global</span>
                        </a>
                    </li>
                
                    <li class="nr-header__link-wrap">
                        <a href="/en_gb/united-kingdom/home/castrol-story/contact-us.html" class="nr-header__link">
                            
                            <i class="nr-header__icon">
                                <img class="nr-icon-arrow nr-header__icon" width="100%" height="100%" src="<?php echo get_theme_file_uri(); ?>/inc/img/CAS002_Complete_CMS-icon-contact.svg" alt="" style="height: 18px">
                            </i>
                            
                            <span class="nr-header__link-text">Contact Us</span>
                        </a>
                    </li>
                </ul>

                
                
                <form action="/en_gb/united-kingdom/home/search-results.html" method="GET" class="nr-header__search-outer-wrap">

                    <div class="nr-header__search">
                        <div class="nr-header__search-inner-wrap">
                            <div class="nr-header__search-icon-wrap">
                                <button type="submit" class="nr-header__search-btn">
                                    <i class="nr-icon-search-2 nr-header__icon"></i>
                                    <span class="nr-visually-hidden">Search</span>
                                </button>
                            </div>
                            <label for="q" class="nr-visually-hidden">Search</label>
                            <input placeholder="Search" id="q" name="q" class="nr-header__input aa-input" type="text" autocomplete="off" spellcheck="false" role="combobox" aria-autocomplete="both" aria-expanded="false" aria-owns="algolia-autocomplete-listbox-2" dir="auto" style=""><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Castrol Sans Western European&quot;, Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: normal; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Castrol Sans Western European&quot;, Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: normal; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre>

                            <a href="#" class="nr-header__icon-close-wrap" aria-label="close search">
                                <svg class="nr-header__icon-close" xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 52 52"><g fill="#FFF" fill-rule="evenodd"><path d="M3.373.544l48.083 48.083-2.83 2.83L.545 3.372z"></path><path d="M1 49.083L49.083 1l2.83 2.828L3.828 51.912z"></path></g></svg>
                            </a>
                        </div>
                    </div>
                    <div data-component-name="searchPopup" class="nr-component nr-search-popup" data-algolia-id="RF87OIMXXP" data-algolia-key="fd6233f7d76d49af576e856909221d49" data-algolia-results="castrol.com" data-algolia-suggestions="castrol_query_suggestions">
                        <div class="nr-search-popup-recent" data-listbox-description="Recent Suggestions"></div>
                        <div class="nr-search-popup-results" data-listbox-description="Search Suggestions"><span class="algolia-autocomplete" style="position: absolute; z-index: 100; display: none; direction: ltr;"><span class="aa-dropdown-menu" role="listbox" id="algolia-autocomplete-listbox-2" title="Search Suggestions" style="display: block; left: 0px; right: auto;"><div class="aa-dataset-3"></div></span></span></div>
                        <div class="nr-search-popup-suggestions" data-listbox-description="Search Result">
                            <div class="nr-search-popup-suggestions-title">Suggestions</div>
                            <div class="nr-search-popup-suggestions-links"><span class="algolia-autocomplete" style="position: absolute; z-index: 100; display: none; direction: ltr;"><span class="aa-dropdown-menu" role="listbox" id="algolia-autocomplete-listbox-0" title="Search Result" style="display: block; left: 0px; right: auto;"><div class="aa-dataset-1"></div></span></span></div>
                            <a href="#" class="nr-search-popup-suggestions-viewall">
                                View all results
                                <i class="nr-icon nr-icon-arrow"></i>
                            </a>
                        </div>

                    </div>
                </form>
                <p class="nr-header__tagline">IT'S MORE THAN JUST OIL. IT'S LIQUID ENGINEERING.</p>
            </div>
        </div>
    </header>   

    
