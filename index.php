<?php 
    get_header();
    
    get_sidebar();

    $page_title = get_the_title();
    $hero_image = get_template_directory_uri() . '/inc/img/cas002-hero-image.jpg';
    


    echo '
        <div data-component-name="pagetitle" class="nr-component nr-page-title aem-GridColumn aem-GridColumn--default--12">
            <div class="nr-row">
                <div class="nr-page-title__wrapper nr-page-title__columns">
                    <div class="nr-page-title__wrap ">
                        <div class="nr-page-title__wrap-inner ">
                            <section class="nr-page-title__header">
                                <h1 class="nr-page-title__header--title">' . $page_title . '</h1>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ';

    while( have_posts() ) :
        the_post();    
            the_content();                    
    endwhile;

    // Flexible Content Loop
    if( have_rows('content') ):

        while ( have_rows('content') ) : the_row();

            if( get_row_layout() == 'hero_image' ):
                get_template_part( 'template-parts/component-hero', 'hero');

            elseif( get_row_layout() == 'text_area' ): 
                get_template_part( 'template-parts/component-text', 'text' );

            elseif( get_row_layout() == 'three_column' ): 
                get_template_part( 'template-parts/component-columns', 'columns' );

            elseif( get_row_layout() == 'divider' ): 
                get_template_part( 'template-parts/divider', 'columns' );

            elseif( get_row_layout() == 'benefits_list' ): 
                get_template_part( 'template-parts/component-benefits', 'benefits' );

            elseif( get_row_layout() == 'features_component' ): 
                get_template_part( 'template-parts/component-features', 'features' );

            elseif( get_row_layout() == 'helping_component' ): 
                get_template_part( 'template-parts/component-helping', 'helping' );
                
            elseif( get_row_layout() == 'cta_component' ): 
                get_template_part( 'template-parts/component-cta', 'cta' );
                
            endif;

        endwhile;

    endif;

    // Pre Footer
    get_template_part( 'template-parts/pre-footer', 'pre-footer' );
            
    get_footer();
?>

<div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">