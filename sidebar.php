<aside class="nr-navigation nr-home">
            
            <div class="nr-navigation__inner ">
                
                <button class="nr-navigation__bypass">Bypass Navigation</button>
                <div class="nr-navigation__controller " style="max-height: 1249px;">
                    <div class="nr-navigation__list-wrapper nr-navigation__list-wrapper--first nr-navigation__list-active">
                        <button aria-label="previous" class="nr-navigation__prev-btn nr-navigation__remove-animate nr-navigation__hide-btn" style=""><i class="nr-icon-arrow"></i><span class="nr-visually-hidden">previous</span></button>
                        
                        <nav class="nr-navigation__container nr-navigation__container--mainlevel" data-level="0" role="navigation">
                            <div class="nr-navigation__item nr-navigation__list-title">
                                <a class="nr-navigation__link" href="/en_gb/united-kingdom/home.html" data-type="link">HOME</a>
                            </div>

                            <script type="text/javascript">
            var navDataArr = [
                
            {
                "path": "/en_gb/united-kingdom/home/car-servicing.html",
                "class": "nr-navigation__link  ",
                "type": "link",
                "title": "SERVICING YOUR CAR WITH CASTROL",
                "index": "0",
                "showIcon": false
            },
                
            {
                "path": "/en_gb/united-kingdom/home/path360.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "SUSTAINABILITY PATH360",
                "index": "1",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/path360/saving-waste.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "SAVING WASTE",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/path360/reducing-carbon.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "REDUCING CARBON",
                        "index": "1",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/path360/improving-peoples-lives.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "IMPROVING PEOPLE'S LIVES",
                        "index": "2",
                        "showIcon": false
                    }
                ]
            },
                
            {
                "path": "/en_gb/united-kingdom/home/castrol-in-space.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "CASTROL IN SPACE",
                "index": "2",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-in-space/mission-milestones.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "MISSION MILESTONES",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-in-space/did-you-know-mars.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "DID YOU KNOW- MARS",
                        "index": "1",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-in-space/castrol-on-earth.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CASTROL ON EARTH",
                        "index": "2",
                        "showIcon": false
                    }
                ]
            }, 
                
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "CAR ENGINE OIL AND FLUIDS",
                "index": "3",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/motor-oil-and-fluids-finder.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CAR ENGINE OIL &amp; FLUIDS FINDER",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "ENGINE OILS",
                        "index": "1",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "ENGINE OIL VISCOSITY GRADES",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/0w-20.html",
                "class": "nr-navigation__link ",
                "title": "0W-20",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/0w-30.html",
                "class": "nr-navigation__link ",
                "title": "0W-30",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/0w-40.html",
                "class": "nr-navigation__link ",
                "title": "0W-40",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/5w-30.html",
                "class": "nr-navigation__link ",
                "title": "5W-30",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/5w-40.html",
                "class": "nr-navigation__link ",
                "title": "5W-40",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/5w-50.html",
                "class": "nr-navigation__link ",
                "title": "5W-50",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/10w-40.html",
                "class": "nr-navigation__link ",
                "title": "10W-40",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/10w-60.html",
                "class": "nr-navigation__link ",
                "title": "10W-60",
                "dataType": "link",
                "index": "7",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-viscosity-grades/15w-40.html",
                "class": "nr-navigation__link ",
                "title": "15W-40",
                "dataType": "link",
                "index": "8",
                "showIcon": false
            }
 ]
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-types.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "ENGINE OIL TYPES",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-types/full-synthetic.html",
                "class": "nr-navigation__link ",
                "title": "FULL SYNTHETIC OIL",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-types/part-synthetic.html",
                "class": "nr-navigation__link ",
                "title": "PART SYNTHETIC OIL",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }
 ]
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "ENGINE OIL BRANDS",
                "dataType": "section",
                "index": "2",
                "showIcon": true,
                "children": [         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-edge-brand.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "CASTROL EDGE OILS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-edge-brand/castrol-edge.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL EDGE",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-edge-brand/castrol-edge-supercar.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL EDGE SUPERCAR",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }
 ]
            },
         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-magnatec-brand.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "CASTROL MAGNATEC OILS",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [         
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-magnatec-brand/castrol-magnatec.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL MAGNATEC",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-magnatec-brand/castrol-magnatec-stop-start.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL MAGNATEC STOP-START",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-gtx-brand.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "CASTROL GTX OILS",
                "dataType": "section",
                "index": "2",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-gtx-brand/castrol-gtx.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL GTX",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-oils/engine-oil-brands/castrol-gtx-brand/castrol-gtx-ultraclean.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL GTX ULTRACLEAN",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                    
                ]
            }

                    
                ]
            }       ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/driveline-fluids.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "DRIVELINE FLUIDS",
                        "index": "2",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/driveline-fluids/automatic-transmission-fluids.html",
                "class": "nr-navigation__link ",
                "title": "AUTOMATIC TRANSMISSION FLUIDS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/driveline-fluids/manual-transmission-fluids.html",
                "class": "nr-navigation__link ",
                "title": "MANUAL TRANSMISSION FLUIDS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/driveline-fluids/axle-and-universal-fluids.html",
                "class": "nr-navigation__link ",
                "title": "AXLE AND UNIVERSAL FLUIDS",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/driveline-fluids/castrol-transmax.html",
                "class": "nr-navigation__link ",
                "title": "DRIVELINE - CASTROL TRANSMAX",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            }       ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/brake-fluids.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "BRAKE FLUIDS",
                        "index": "3",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/coolants.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "COOLANTS",
                        "index": "4",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/engine-shampoo.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CASTROL ENGINE SHAMPOO",
                        "index": "5",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/greases.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "GREASES",
                        "index": "6",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/castrol-classic-oils.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CASTROL CLASSIC OILS",
                        "index": "7",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/car-engine-maintenance.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "CAR ENGINE MAINTENANCE",
                        "index": "8",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/car-engine-maintenance/how-often-should-you-check-your-engine-oil.html",
                "class": "nr-navigation__link ",
                "title": "HOW OFTEN SHOULD YOU CHECK YOUR ENGINE OIL",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/car-engine-maintenance/how-to-check-your-engine-oil.html",
                "class": "nr-navigation__link ",
                "title": "HOW TO CHECK YOUR ENGINE OIL",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/car-engine-maintenance/how-to-top-up-your-engine-oil.html",
                "class": "nr-navigation__link ",
                "title": "HOW TO TOP UP YOUR CAR ENGINE OIL",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/car-engine-maintenance/why-you-should-use-synthetic-oil.html",
                "class": "nr-navigation__link ",
                "title": "WHY YOU SHOULD USE SYNTHETIC OIL",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/car-engine-maintenance/how-to-recycle-your-used-engine-oil.html",
                "class": "nr-navigation__link ",
                "title": "HOW TO RECYCLE YOUR USED ENGINE OIL",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            }       ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/find-a-castrol-dealership-or-workshop.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "FIND A CASTROL DEALERSHIP OR OIL CHANGE WORKSHOP",
                        "index": "9",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/castrol-engine-warranty.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "CASTROL ENGINE WARRANTY",
                        "index": "10",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/castrol-engine-warranty/registration-page.html",
                "class": "nr-navigation__link ",
                "title": "ENGINE WARRANTY REGISTRATION",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/castrol-engine-warranty/frequently-asked-questions.html",
                "class": "nr-navigation__link ",
                "title": "FREQUENTLY ASKED QUESTIONS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/castrol-engine-warranty/terms-and-conditions.html",
                "class": "nr-navigation__link ",
                "title": "TERMS AND CONDITIONS",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },               
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/castrol-engine-warranty/privacy-policy.html",
                "class": "nr-navigation__link ",
                "title": "PRIVACY POLICY",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/car-engine-oil-and-fluids/castrol-engine-warranty/claim-form.html",
                "class": "nr-navigation__link ",
                "title": "ONLINE CLAIM FORM",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            }

                            
                        ]
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "MOTORCYCLE OIL &amp; FLUIDS",
                "index": "4",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motor-oil---fluids-finder.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "MOTORCYCLE OIL &amp; FLUIDS FINDER",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-engine-oils.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "MOTORCYCLE ENGINE OILS",
                        "index": "1",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-engine-oils/2-stroke-engine-oils.html",
                "class": "nr-navigation__link ",
                "title": "2-STROKE ENGINE OILS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-engine-oils/4-stroke-engine-oils.html",
                "class": "nr-navigation__link ",
                "title": "4-STROKE ENGINE OILS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-engine-oils/engine-oil-by-brand.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "ENGINE OIL BY BRAND",
                "dataType": "section",
                "index": "2",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-engine-oils/engine-oil-by-brand/castrol-power1.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL POWER1",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-engine-oils/engine-oil-by-brand/castrol-power1-racing.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL POWER1 RACING",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-engine-oils/engine-oil-by-brand/castrol-power1-scooter.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL POWER1 SCOOTER",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-speciality-products.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "MOTORCYCLE SPECIALITY PRODUCTS",
                        "index": "2",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "MOTORCYCLE MAINTENANCE",
                        "index": "3",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance/how-often-to-check-motorcycle-oil.html",
                "class": "nr-navigation__link ",
                "title": "HOW OFTEN TO CHECK MOTORCYCLE OIL",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance/how-to-check-motorcycle-oil.html",
                "class": "nr-navigation__link ",
                "title": "HOW TO CHECK MOTORCYCLE OIL",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance/checking-oil-through-the-inspection-window.html",
                "class": "nr-navigation__link ",
                "title": "CHECKING OIL THROUGH THE INSPECTION WINDOW",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance/checking-motorcycle-oil-with-a-dipstick.html",
                "class": "nr-navigation__link ",
                "title": "CHECKING MOTORCYCLE OIL WITH A DIPSTICK",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance/topping-up-motorcycle-oil.html",
                "class": "nr-navigation__link ",
                "title": "TOPPING UP MOTORCYCLE OIL",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance/checking-your-motorcycles-brake-fluid.html",
                "class": "nr-navigation__link ",
                "title": "CHECKING YOUR MOTORCYCLE'S BRAKE FLUID",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/motorcycle-maintenance/recycling-your-used-motorcycle-oil.html",
                "class": "nr-navigation__link ",
                "title": "RECYCLING YOUR USED MOTORCYCLE OIL",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/motorcycle-oil-and-fluids/find-a-castrol-dealership-or-workshop.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "FIND A CASTROL DEALERSHIP OR OIL CHANGE WORKSHOP",
                        "index": "4",
                        "showIcon": false
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "COMMERCIAL VEHICLE OIL &amp; FLUIDS",
                "index": "5",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/applications.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "APPLICATIONS",
                        "index": "0",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/applications/on-road.html",
                "class": "nr-navigation__link ",
                "title": "ON ROAD TRANSPORT",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/applications/agriculture.html",
                "class": "nr-navigation__link ",
                "title": "AGRICULTURE",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/applications/construction-and-landfill.html",
                "class": "nr-navigation__link ",
                "title": "CONSTRUCTION &amp; LANDFILL",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/applications/mining-and-quarrying.html",
                "class": "nr-navigation__link ",
                "title": "MINING &amp; QUARRYING",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "PRODUCT TYPES",
                        "index": "1",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "ENGINE OILS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-viscosity-grades.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "DIESEL ENGINE OIL VISCOSITY GRADES",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-viscosity-grades/5w-30.html",
                "class": "nr-navigation__link ",
                "title": "5W-30",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-viscosity-grades/10w-30.html",
                "class": "nr-navigation__link ",
                "title": "10W-30",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-viscosity-grades/10w-40.html",
                "class": "nr-navigation__link ",
                "title": "10W-40",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-viscosity-grades/15w-40.html",
                "class": "nr-navigation__link ",
                "title": "15W-40",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-viscosity-grades/monograde.html",
                "class": "nr-navigation__link ",
                "title": "MONOGRADE",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "DIESEL ENGINE OIL SPECIFICATIONS &amp; OEM APPROVALS",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/api.html",
                "class": "nr-navigation__link ",
                "title": "API",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/jaso.html",
                "class": "nr-navigation__link ",
                "title": "JASO",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/cat.html",
                "class": "nr-navigation__link ",
                "title": "CAT",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/cummins.html",
                "class": "nr-navigation__link ",
                "title": "CUMMINS",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/deutz.html",
                "class": "nr-navigation__link ",
                "title": "DEUTZ",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/mack.html",
                "class": "nr-navigation__link ",
                "title": "MACK",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/mb.html",
                "class": "nr-navigation__link ",
                "title": "MB",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/renault.html",
                "class": "nr-navigation__link ",
                "title": "RENAULT",
                "dataType": "link",
                "index": "7",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/volvo.html",
                "class": "nr-navigation__link ",
                "title": "VOLVO",
                "dataType": "link",
                "index": "8",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/acea.html",
                "class": "nr-navigation__link ",
                "title": "ACEA",
                "dataType": "link",
                "index": "9",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/man.html",
                "class": "nr-navigation__link ",
                "title": "MAN",
                "dataType": "link",
                "index": "10",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/scania.html",
                "class": "nr-navigation__link ",
                "title": "SCANIA",
                "dataType": "link",
                "index": "11",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-specifications/mtu.html",
                "class": "nr-navigation__link ",
                "title": "MTU",
                "dataType": "link",
                "index": "12",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-brands.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "COMMERCIAL VEHICLE ENGINE OIL BRANDS",
                "dataType": "section",
                "index": "2",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-brands/castrol-vecton.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL VECTON",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/engine-oils/engine-oil-brands/castrol-crb.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL CRB",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                    
                ]
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/driveline-fluids.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "DRIVELINE FLUIDS",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/driveline-fluids/automatic-transmission-fluids.html",
                "class": "nr-navigation__link ",
                "title": "AUTOMATIC TRANSMISSION FLUIDS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/driveline-fluids/manual-transmission-fluids.html",
                "class": "nr-navigation__link ",
                "title": "MANUAL TRANSMISSION FLUIDS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/driveline-fluids/axle-and-universal-fluids.html",
                "class": "nr-navigation__link ",
                "title": "AXLE AND UNIVERSAL FLUIDS",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/coolants.html",
                "class": "nr-navigation__link ",
                "title": "COOLANTS",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/greases.html",
                "class": "nr-navigation__link ",
                "title": "GREASES",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids/product-types/hydraulic-fluids.html",
                "class": "nr-navigation__link ",
                "title": "HYDRAULIC FLUIDS",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            }

                            
                        ]
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/electric-vehicle-fluids.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "ELECTRIC VEHICLE FLUIDS",
                "index": "6",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/electric-vehicle-fluids/castrol-transmission-e-fluids.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "TRANSMISSION E-FLUIDS",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/electric-vehicle-fluids/thermal-management-e-fluids.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "THERMAL MANAGEMENT E-FLUIDS",
                        "index": "1",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/electric-vehicle-fluids/castrol-grease-e-fluids.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "GREASE E-FLUIDS",
                        "index": "2",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/electric-vehicle-fluids/castrol-partnerships.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "OUR PARTNERSHIPS",
                        "index": "3",
                        "showIcon": false
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "AUTOMOTIVE REPAIR WORKSHOPS",
                "index": "7",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/auto-repair-workshops/car-servicing.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CAR SERVICING",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "INDEPENDENT WORKSHOPS",
                        "index": "1",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/online-training.html",
                "class": "nr-navigation__link ",
                "title": "ONLINE TRAINING ",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/branded-workshops.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "BRANDED WORKSHOPS",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/branded-workshops/register-your-interest.html",
                "class": "nr-navigation__link ",
                "title": "REGISTER YOUR INTEREST",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/mobile-mechanic.html",
                "class": "nr-navigation__link ",
                "title": "MOBILE MECHANIC",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/connect.html",
                "class": "nr-navigation__link ",
                "title": "BRAND SUPPORT",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/technical-support.html",
                "class": "nr-navigation__link ",
                "title": "IWS TECHNICAL SUPPORT",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/castrol-incentive-plus.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL INCENTIVE PLUS",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/independent-workshops/castrol-webinars.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL WEBINARS",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/auto-repair-workshops/franchise-workshops.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "FRANCHISE WORKSHOPS",
                        "index": "2",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/auto-repair-workshops/franchise-workshops/online-training.html",
                "class": "nr-navigation__link ",
                "title": "ONLINE TRAINING",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/auto-repair-workshops/uk-distributors.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "UK DISTRIBUTORS",
                        "index": "3",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/auto-repair-workshops/ireland-distributors.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "IRELAND DISTRIBUTORS",
                        "index": "4",
                        "showIcon": false
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/industrial.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "INDUSTRIAL",
                "index": "8",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/industrial/latest-products.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "LATEST PRODUCTS ",
                        "index": "0",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/xbb-technology.html",
                "class": "nr-navigation__link ",
                "title": "XBB TECHNOLOGY",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/alusol.html",
                "class": "nr-navigation__link ",
                "title": "ALUSOL",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/brayco.html",
                "class": "nr-navigation__link ",
                "title": "BRAYCO",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/hysol.html",
                "class": "nr-navigation__link ",
                "title": "HYSOL",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/iloform-cfx.html",
                "class": "nr-navigation__link ",
                "title": "ILOFORM CFX",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/molub-alloy.html",
                "class": "nr-navigation__link ",
                "title": "MOLUB-ALLOY",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/on-car-lubricants.html",
                "class": "nr-navigation__link ",
                "title": "ON CAR LUBRICANTS",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/optigear.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "OPTIGEAR",
                "dataType": "section",
                "index": "7",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/optigear/optigear-wind.html",
                "class": "nr-navigation__link ",
                "title": "OPTIGEAR WIND",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/performance-bio.html",
                "class": "nr-navigation__link ",
                "title": "PERFORMANCE BIO",
                "dataType": "link",
                "index": "8",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/industrial-robotics.html",
                "class": "nr-navigation__link ",
                "title": "ROBOTICS",
                "dataType": "link",
                "index": "9",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/syntilo.html",
                "class": "nr-navigation__link ",
                "title": "SYNTILO",
                "dataType": "link",
                "index": "10",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/techniclean.html",
                "class": "nr-navigation__link ",
                "title": "TECHNICLEAN XBC",
                "dataType": "link",
                "index": "11",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/tribol.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "TRIBOL",
                "dataType": "section",
                "index": "12",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/latest-products/tribol/tribol-wind.html",
                "class": "nr-navigation__link ",
                "title": "TRIBOL WIND",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                    
                ]
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "LUBRICANTS BY PRODUCT TYPE",
                        "index": "1",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "METALWORKING",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking/cleaners.html",
                "class": "nr-navigation__link ",
                "title": "CLEANERS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking/corrosion-preventives.html",
                "class": "nr-navigation__link ",
                "title": "CORROSION PREVENTIVES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking/forming-fluids.html",
                "class": "nr-navigation__link ",
                "title": "FORMING FLUIDS",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking/quenching-fluids.html",
                "class": "nr-navigation__link ",
                "title": "QUENCHING FLUIDS",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking/neat-cutting-fluids.html",
                "class": "nr-navigation__link ",
                "title": "NEAT CUTTING FLUIDS",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking/soluble-cutting-fluids.html",
                "class": "nr-navigation__link ",
                "title": "SOLUBLE CUTTING FLUIDS",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/metalworking/synthetic-cutting-fluids.html",
                "class": "nr-navigation__link ",
                "title": "SYNTHETIC CUTTING FLUIDS",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/lubricants.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "LUBRICANTS",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/lubricants/chain-oils.html",
                "class": "nr-navigation__link ",
                "title": "CHAIN OILS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/lubricants/compressor-oils.html",
                "class": "nr-navigation__link ",
                "title": "COMPRESSOR OILS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/lubricants/gear-oils.html",
                "class": "nr-navigation__link ",
                "title": "GEAR OILS",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/lubricants/greases.html",
                "class": "nr-navigation__link ",
                "title": "GREASES",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/lubricants/hydraulic-fluids.html",
                "class": "nr-navigation__link ",
                "title": "HYDRAULIC FLUIDS",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-product-type/lubricants/turbine-oils.html",
                "class": "nr-navigation__link ",
                "title": "TURBINE OILS",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            }

                    
                ]
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "LUBRICANTS BY SECTOR",
                        "index": "2",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "AEROSPACE MANUFACTURE &amp; MAINTENANCE",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/applications.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "APPLICATIONS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/applications/airframe.html",
                "class": "nr-navigation__link ",
                "title": "AIRFRAME",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/applications/assembly.html",
                "class": "nr-navigation__link ",
                "title": "ASSEMBLY",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/applications/components.html",
                "class": "nr-navigation__link ",
                "title": "COMPONENTS",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/applications/engines.html",
                "class": "nr-navigation__link ",
                "title": "ENGINES",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/applications/maintenance-first-fill.html",
                "class": "nr-navigation__link ",
                "title": "MAINTENANCE &amp; FIRST FILL",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/applications/space.html",
                "class": "nr-navigation__link ",
                "title": "SPACE",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/product-types.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT TYPES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/aerospace/services.html",
                "class": "nr-navigation__link ",
                "title": "OFFERS &amp; SERVICES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "AUTOMOTIVE MANUFACTURE",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture/applications.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "APPLICATIONS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture/applications/engine-and-transmission.html",
                "class": "nr-navigation__link ",
                "title": "ENGINE AND TRANSMISSION",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture/applications/component-manufacture-and-lubrication.html",
                "class": "nr-navigation__link ",
                "title": "COMPONENT MANUFACTURE &amp; LUBRICATION",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture/applications/assembly-and-paint-shop.html",
                "class": "nr-navigation__link ",
                "title": "ASSEMBLY AND PAINT SHOP",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture/applications/press-and-body.html",
                "class": "nr-navigation__link ",
                "title": "PRESS &amp; BODY",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture/product-types.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT TYPES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/automotive-manufacture/services.html",
                "class": "nr-navigation__link ",
                "title": "OFFERS &amp; SERVICES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "MACHINERY MANUFACTURING",
                "dataType": "section",
                "index": "2",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery/applications.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "APPLICATIONS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery/applications/bearings.html",
                "class": "nr-navigation__link ",
                "title": "BEARINGS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery/applications/gears.html",
                "class": "nr-navigation__link ",
                "title": "GEARS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery/applications/electrical-and-electronic.html",
                "class": "nr-navigation__link ",
                "title": "ELECTRICAL AND ELECTRONIC",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery/applications/pumps-compressors-refrigeration-and-air-conditioning.html",
                "class": "nr-navigation__link ",
                "title": " PUMPS, COMPRESSORS, REFRIGERATION AND AIR CONDITIONING ",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery/product-types.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT TYPES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/machinery/services.html",
                "class": "nr-navigation__link ",
                "title": "OFFERS &amp; SERVICES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/metals-manufacturing.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "METALS MANUFACTURING",
                "dataType": "section",
                "index": "3",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/metals-manufacturing/applications.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "APPLICATIONS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/metals-manufacturing/applications/iron-and-steel.html",
                "class": "nr-navigation__link ",
                "title": "IRON &amp; STEEL",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/metals-manufacturing/product-types.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT TYPES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/metals-manufacturing/services.html",
                "class": "nr-navigation__link ",
                "title": "OFFERS &amp; SERVICES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/mining.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "MINING",
                "dataType": "section",
                "index": "4",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/mining/applications.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "APPLICATIONS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/mining/applications/open-cast.html",
                "class": "nr-navigation__link ",
                "title": "OPEN CAST",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/mining/applications/processing.html",
                "class": "nr-navigation__link ",
                "title": "PROCESSING",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/mining/product-types.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT TYPES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/mining/services.html",
                "class": "nr-navigation__link ",
                "title": "OFFERS &amp; SERVICES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/wind.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "WIND",
                "dataType": "section",
                "index": "5",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/wind/applications.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "APPLICATIONS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/wind/applications/wind-turbine-operation-and-maintenance.html",
                "class": "nr-navigation__link ",
                "title": "WIND TURBINE OPERATION &amp; MAINTENANCE",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/wind/product-types.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT TYPES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/wind/services.html",
                "class": "nr-navigation__link ",
                "title": "OFFERS &amp; SERVICES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "OTHER INDUSTRIES",
                "dataType": "section",
                "index": "6",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "OTHER SECTORS",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors/chemicals.html",
                "class": "nr-navigation__link ",
                "title": "CHEMICALS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors/engineered-wood.html",
                "class": "nr-navigation__link ",
                "title": "ENGINEERED WOOD",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors/food-and-beverage.html",
                "class": "nr-navigation__link ",
                "title": "FOOD &amp; BEVERAGE",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors/metal-goods.html",
                "class": "nr-navigation__link ",
                "title": "METAL GOODS",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors/power-generation.html",
                "class": "nr-navigation__link ",
                "title": "POWER GENERATION",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors/pulp-and-paper.html",
                "class": "nr-navigation__link ",
                "title": "PULP &amp; PAPER",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/other-sectors/sugar.html",
                "class": "nr-navigation__link ",
                "title": "SUGAR",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            }

                    
                ]
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/product-types.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT TYPES",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/industrial/lubricants-by-sector/other-industries/services.html",
                "class": "nr-navigation__link ",
                "title": "OFFERS &amp; SERVICES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                    
                ]
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/industrial/offers-and-services.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "OFFERS AND SERVICES",
                        "index": "3",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/used-oil-analysis.html",
                "class": "nr-navigation__link ",
                "title": "USED OIL ANALYSIS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/lubrication-scheduling.html",
                "class": "nr-navigation__link ",
                "title": "LUBRICATION SCHEDULING",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/labcheck.html",
                "class": "nr-navigation__link ",
                "title": "LABCHECK",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/techservice.html",
                "class": "nr-navigation__link ",
                "title": "TECHSERVICE",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/predict.html",
                "class": "nr-navigation__link ",
                "title": "PREDICT",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/simultaneous-engineering.html",
                "class": "nr-navigation__link ",
                "title": "SIMULTANEOUS ENGINEERING",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/complete-cms.html",
                "class": "nr-navigation__link ",
                "title": "COMPLETE CMS",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/wind-after-market.html",
                "class": "nr-navigation__link ",
                "title": "WIND AFTER MARKET",
                "dataType": "link",
                "index": "7",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/smart-control.html",
                "class": "nr-navigation__link ",
                "title": "SMART CONTROL",
                "dataType": "link",
                "index": "8",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/castrol-academy.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL WIND ACADEMY",
                "dataType": "link",
                "index": "9",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/windenergy.html",
                "class": "nr-navigation__link ",
                "title": "WIND 360 SOLUTIONS",
                "dataType": "link",
                "index": "10",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/offers-and-services/smart-sensor.html",
                "class": "nr-navigation__link ",
                "title": "SMARTSENSOR",
                "dataType": "link",
                "index": "11",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/industrial/industrial-case-studies.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CASE STUDIES",
                        "index": "4",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/industrial/whitepapers.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "WHITEPAPERS",
                        "index": "5",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/whitepapers/smart-control-white-paper.html",
                "class": "nr-navigation__link ",
                "title": "SMART CONTROL WHITE PAPER",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/whitepapers/techniclean-white-paper.html",
                "class": "nr-navigation__link ",
                "title": "TECHNICLEAN XBC WHITE PAPER",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/industrial/whitepapers/iloform-white-paper.html",
                "class": "nr-navigation__link ",
                "title": "ILOFORM WHITE PAPER",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                            
                        ]
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/marine.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "MARINE",
                "index": "9",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/smartgains.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "SMARTGAINS",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/newsroom.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "NEWSROOM",
                        "index": "1",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/castrol-marine-2020.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "IMO 2020",
                        "index": "2",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/castrol-marine-2020/contact-us.html",
                "class": "nr-navigation__link ",
                "title": "CONTACT US",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/applications.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "MEETING YOUR NEEDS",
                        "index": "3",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/applications/international-trading-vessels.html",
                "class": "nr-navigation__link ",
                "title": "INTERNATIONAL TRADING VESSELS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/applications/specialist-vessels.html",
                "class": "nr-navigation__link ",
                "title": "SPECIALIST VESSELS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/services.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "OFFERS &amp; SERVICES",
                        "index": "4",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/services/caremax-oil-monitor.html",
                "class": "nr-navigation__link ",
                "title": "CAREMAX OIL MONITOR",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/services/caremax-stern-tube.html",
                "class": "nr-navigation__link ",
                "title": "CAREMAX STERN TUBE",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/services/caremax-hydraulic.html",
                "class": "nr-navigation__link ",
                "title": "CAREMAX HYDRAULIC",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/services/caremax-machinery-health-monitor.html",
                "class": "nr-navigation__link ",
                "title": "CAREMAX MACHINERY HEALTH MONITOR",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/services/caremax-lube-oil-test.html",
                "class": "nr-navigation__link ",
                "title": "CAREMAX LUBE OIL TEST",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/services/caremax-water-test-kit.html",
                "class": "nr-navigation__link ",
                "title": " CAREMAX WATER TEST KIT",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "PRODUCTS &amp; APPLICATIONS",
                        "index": "5",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "BIO RANGE",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range/castrol-biostat.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL BIOSTAT",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range/castrol-biobar.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL BIOBAR",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range/castrol_biotac_mp.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL BIOTAC MP",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range/environmental-performance.html",
                "class": "nr-navigation__link ",
                "title": "ENVIRONMENTAL PERFORMANCE",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range/technical-performance.html",
                "class": "nr-navigation__link ",
                "title": "TECHNICAL PERFORMANCE",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range/our-approach.html",
                "class": "nr-navigation__link ",
                "title": "OUR APPROACH",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/bio-range/environmental-legislation-us-vessel-general-permit.html",
                "class": "nr-navigation__link ",
                "title": "ENVIRONMENTAL LEGISLATION / US VESSEL GENERAL PERMIT",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/hydraulic-systems.html",
                "class": "nr-navigation__link ",
                "title": "HYDRAULIC SYSTEMS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/gearboxes.html",
                "class": "nr-navigation__link ",
                "title": "GEARBOXES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/air-compressor.html",
                "class": "nr-navigation__link ",
                "title": "AIR COMPRESSORS",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/gas-compressors.html",
                "class": "nr-navigation__link ",
                "title": "GAS COMPRESSORS",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/refrigeration-systems.html",
                "class": "nr-navigation__link ",
                "title": "REFRIGERATION SYSTEMS",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/turbochargers.html",
                "class": "nr-navigation__link ",
                "title": "TURBOCHARGERS",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/grease-applications.html",
                "class": "nr-navigation__link ",
                "title": "GREASE APPLICATIONS",
                "dataType": "link",
                "index": "7",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/stern-tubes-and-thrusters.html",
                "class": "nr-navigation__link ",
                "title": "STERN TUBES AND THRUSTERS",
                "dataType": "link",
                "index": "8",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/lubricants-by-product-type/turbines.html",
                "class": "nr-navigation__link ",
                "title": "TURBINES",
                "dataType": "link",
                "index": "9",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/trusted-advice.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "TRUSTED ADVICE",
                        "index": "6",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/health-safety-environment.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "HEALTH, SAFETY, ENVIRONMENT",
                        "index": "7",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/health-safety-environment/eca-legislation.html",
                "class": "nr-navigation__link ",
                "title": "ECA LEGISLATION",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/marine/health-safety-environment/environmental-legislation-us-vessel-general-permit.html",
                "class": "nr-navigation__link ",
                "title": "ENVIRONMENTAL LEGISLATION / US VESSEL GENERAL PERMIT",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/supply-delivery.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "SUPPLY &amp; DELIVERY",
                        "index": "8",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/marine/castrol-academy.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CASTROL ACADEMY",
                        "index": "9",
                        "showIcon": false
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "OIL &amp; GAS",
                "index": "10",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/smartgains.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "SMARTGAINS",
                        "index": "0",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/applications.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "MEETING YOUR NEEDS",
                        "index": "1",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/applications/subsea-production.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "SUBSEA PRODUCTION",
                "dataType": "section",
                "index": "0",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/applications/subsea-production/castrol-transaqua-sp.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL TRANSAQUA SP",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                    
                ]
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/applications/surface-production.html",
                "class": "nr-navigation__link ",
                "title": "SURFACE PRODUCTION",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/applications/offshore-drilling.html",
                "class": "nr-navigation__link ",
                "title": "OFFSHORE DRILLING",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/applications/onshore-drilling.html",
                "class": "nr-navigation__link ",
                "title": "ONSHORE DRILLING",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/services.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "OFFERS &amp; SERVICES",
                        "index": "2",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "PRODUCTS &amp; APPLICATIONS",
                        "index": "3",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/air-compressor.html",
                "class": "nr-navigation__link ",
                "title": "AIR COMPRESSORS",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/gas-compressors.html",
                "class": "nr-navigation__link ",
                "title": "GAS COMPRESSORS",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/diesel-engines.html",
                "class": "nr-navigation__link ",
                "title": "DIESEL ENGINES",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/gearboxes.html",
                "class": "nr-navigation__link ",
                "title": "GEARBOXES",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/grease-applications.html",
                "class": "nr-navigation__link ",
                "title": "GREASE APPLICATIONS",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/hydraulic-systems.html",
                "class": "nr-navigation__link ",
                "title": "HYDRAULIC SYSTEMS",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/propulsion.html",
                "class": "nr-navigation__link ",
                "title": "PROPULSION",
                "dataType": "link",
                "index": "6",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/refrigeration-compressors.html",
                "class": "nr-navigation__link ",
                "title": "REFRIGERATION COMPRESSORS",
                "dataType": "link",
                "index": "7",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/seal-oil-systems.html",
                "class": "nr-navigation__link ",
                "title": "SEAL OIL SYSTEMS",
                "dataType": "link",
                "index": "8",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/transformer-oils.html",
                "class": "nr-navigation__link ",
                "title": "TRANSFORMER OILS",
                "dataType": "link",
                "index": "9",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/transmission.html",
                "class": "nr-navigation__link ",
                "title": "TRANSMISSION",
                "dataType": "link",
                "index": "10",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/turbines.html",
                "class": "nr-navigation__link ",
                "title": "TURBINES",
                "dataType": "link",
                "index": "11",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/workshop-and-maintenance.html",
                "class": "nr-navigation__link ",
                "title": "WORKSHOP &amp; MAINTENANCE",
                "dataType": "link",
                "index": "12",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/bio-range.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "BIO RANGE",
                "dataType": "section",
                "index": "13",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/bio-range/technical-performance.html",
                "class": "nr-navigation__link ",
                "title": "TECHNICAL PERFORMANCE",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/bio-range/our-approach.html",
                "class": "nr-navigation__link ",
                "title": "OUR APPROACH",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/bio-range/environmental-performance.html",
                "class": "nr-navigation__link ",
                "title": "ENVIRONMENTAL PERFORMANCE",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/bio-range/castrol-biotac-og.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL BIOTAC OG",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/bio-range/castrol-biostat.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL BIOSTAT",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/lubricants-by-product-type/bio-range/castrol-biobar.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL BIOBAR",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            }

                    
                ]
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/trusted-advice.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "TRUSTED ADVICE",
                        "index": "4",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/health-safety-environment.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "HEALTH, SAFETY, ENVIRONMENT",
                        "index": "5",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/health-safety-environment/ospar.html",
                "class": "nr-navigation__link ",
                "title": "OSPAR",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/oil-and-gas/health-safety-environment/vessel-general-permit.html",
                "class": "nr-navigation__link ",
                "title": "VESSEL GENERAL PERMIT",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/packaging.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "PACKAGING",
                        "index": "6",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/delivery.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "DELIVERY",
                        "index": "7",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/oil-and-gas/contact-and-support.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CONTACT OUR EXPERTS",
                        "index": "8",
                        "showIcon": false
                    }
                ]
            },

            
                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story.html",
                "class": "nr-navigation__link  ",
                "type": "section",
                "title": "CASTROL STORY",
                "index": "11",
                "showIcon": true,
                "children": [
                    {
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-story/newsroom.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "NEWSROOM",
                        "index": "0",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/newsroom/features.html",
                "class": "nr-navigation__link ",
                "title": "FEATURES",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/newsroom/press-releases.html",
                "class": "nr-navigation__link nr-navigation__link--section ",
                "title": "PRESS RELEASES",
                "dataType": "section",
                "index": "1",
                "showIcon": true,
                "children": [
                    
                        
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/newsroom/press-releases/path360.html",
                "class": "nr-navigation__link ",
                "title": "PATH360",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                    
                        
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/newsroom/press-releases/castrol-announces-a-new-report-driving-the-evolution.html",
                "class": "nr-navigation__link ",
                "title": "CASTROL ANNOUNCES A NEW REPORT, DRIVING THE EVOLUTION",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            }

                    
                ]
            }
    
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-story/our-heritage.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "OUR HERITAGE",
                        "index": "1",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-story/sustainability.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "SUSTAINABILITY",
                        "index": "2",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/sustainability/taking-action.html",
                "class": "nr-navigation__link ",
                "title": "TAKING ACTION",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-story/careers.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "CAREERS",
                        "index": "3",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/careers/who-we-are.html",
                "class": "nr-navigation__link ",
                "title": "WHO WE ARE",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/careers/what-we-do.html",
                "class": "nr-navigation__link ",
                "title": "WHAT WE DO",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/careers/why-work-for-castrol.html",
                "class": "nr-navigation__link ",
                "title": "WHY WORK FOR CASTROL",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/careers/our-graduate-programmes.html",
                "class": "nr-navigation__link ",
                "title": "OUR GRADUATE PROGRAMMES",
                "dataType": "link",
                "index": "3",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/careers/where-we-work.html",
                "class": "nr-navigation__link ",
                "title": "WHERE WE WORK ",
                "dataType": "link",
                "index": "4",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/careers/apply.html",
                "class": "nr-navigation__link ",
                "title": "APPLY",
                "dataType": "link",
                "index": "5",
                "showIcon": false
            }

                            
                        ]
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-story/contact-us.html",
                        "class": "nr-navigation__link ",
                        "dataType": "link",
                        "title": "CONTACT US",
                        "index": "4",
                        "showIcon": false
                    },{
                        "level": "1",
                        "path": "/en_gb/united-kingdom/home/castrol-story/hse-policy.html",
                        "class": "nr-navigation__link nr-navigation__link--section ",
                        "dataType": "section",
                        "title": "HSE POLICY",
                        "index": "5",
                        "showIcon": true,
                        "children": [
                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/hse-policy/product-stewardship.html",
                "class": "nr-navigation__link ",
                "title": "PRODUCT STEWARDSHIP",
                "dataType": "link",
                "index": "0",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/hse-policy/reach.html",
                "class": "nr-navigation__link ",
                "title": "REACH",
                "dataType": "link",
                "index": "1",
                "showIcon": false
            },

                            
                                
            {
                "path": "/en_gb/united-kingdom/home/castrol-story/hse-policy/ghs.html",
                "class": "nr-navigation__link ",
                "title": "GLOBALLY HARMONIZED SYSTEM",
                "dataType": "link",
                "index": "2",
                "showIcon": false
            }

                            
                        ]
                    }
                ]
            }

        ]
    </script>

    <?php

        

        echo '
                <ul class="nr-navigation__list">            
                                            
                    <li class="nr-navigation__item">
                        <a href="/en_gb/united-kingdom/home/path360.html" class="nr-navigation__link" data-type="section" data-index="1">
                        car engine oil and fluids
                            <i class="nr-icon">
                                <img src="' . get_theme_file_uri() . '/inc/img/CAS002_Complete_CMS-icon-navarrow.svg" style="height: 10px; top: -3px; position: relative;" />
                            </i>
                        </a>
                    </li>  

                    <li class="nr-navigation__item">
                        <a href="/en_gb/united-kingdom/home/path360.html" class="nr-navigation__link" data-type="section" data-index="1">
                            motorcycle oil &amp; fluids 
                            <i class="nr-icon">
                                <img src="' . get_theme_file_uri() . '/inc/img/CAS002_Complete_CMS-icon-navarrow.svg" style="height: 10px; top: -3px; position: relative;" />
                            </i>
                        </a>
                    </li>                 
                                                
                    <li class="nr-navigation__item">
                        <a href="/en_gb/united-kingdom/home/path360.html" class="nr-navigation__link" data-type="section" data-index="1">
                            commercial vehicle oil &amp; fluids 
                            <i class="nr-icon">
                                <img src="' . get_theme_file_uri() . '/inc/img/CAS002_Complete_CMS-icon-navarrow.svg" style="height: 10px; top: -3px; position: relative;" />
                            </i>
                        </a>
                    </li>

                    <li class="nr-navigation__item">
                        <a href="/en_gb/united-kingdom/home/path360.html" class="nr-navigation__link" data-type="section" data-index="1">
                            automotive repair workshops
                            <i class="nr-icon">
                                <img src="' . get_theme_file_uri() . '/inc/img/CAS002_Complete_CMS-icon-navarrow.svg" style="height: 10px; top: -3px; position: relative;" />
                            </i>
                        </a>
                    </li>

                    <li class="nr-navigation__item">
                        <a href="/en_gb/united-kingdom/home/path360.html" class="nr-navigation__link" data-type="section" data-index="1">
                            industrial
                            <i class="nr-icon">
                                <img src="' . get_theme_file_uri() . '/inc/img/CAS002_Complete_CMS-icon-navarrow.svg" style="height: 10px; top: -3px; position: relative;" />
                            </i>
                        </a>
                    </li>

                    <li class="nr-navigation__item">
                        <a href="/en_gb/united-kingdom/home/path360.html" class="nr-navigation__link" data-type="section" data-index="1">
                            marine
                            <i class="nr-icon">
                                <img src="' . get_theme_file_uri() . '/inc/img/CAS002_Complete_CMS-icon-navarrow.svg" style="height: 10px; top: -3px; position: relative;" />
                            </i>
                        </a>
                    </li>

                    <li class="nr-navigation__item">
                        <a href="/en_gb/united-kingdom/home/path360.html" class="nr-navigation__link" data-type="section" data-index="1">
                            oil & gas
                            <i class="nr-icon">
                                <img src="' . get_theme_file_uri() . '/inc/img/CAS002_Complete_CMS-icon-navarrow.svg" style="height: 10px; top: -3px; position: relative;" />
                            </i>
                        </a>
                    </li>
                </ul>

                                        
                <ul class="nr-navigation__borrowed-links">
                    <li class="nr-navigation__borrowed-link">
                        <a class="nr-navigation__link" data-type="link" href="/en_gb/united-kingdom/home/site-tunnel.html">
                            
                                <i class="nr-icon nr-icon-generic nr-navigation__link-icon"></i>
                            
                            <span>Global</span>
                        </a>
                    </li>
                
                    <li class="nr-navigation__borrowed-link">
                        <a class="nr-navigation__link" data-type="link" href="/en_gb/united-kingdom/home/castrol-story/contact-us.html">
                            
                                <i class="nr-icon nr-icon-envelope-2 nr-navigation__link-icon"></i>
                            
                            <span>Contact Us</span>
                        </a>
                    </li>
                </ul>
                                        
            </nav>

                <button aria-label="next" class="nr-navigation__next-btn nr-navigation__hide-btn nr-navigation__remove-animate" style="top: 630px;"><i class="nr-icon-arrow"></i><span class="nr-visually-hidden">next</span></button>
                </div>
                <div class="nr-navigation__footer">
                    
                        <div class="nr-navigation__quick-links">
                            <ul class="nr-navigation__list nr-navigation__list--bottom">
                                <li class="nr-navigation__list-item">
                                    <a href="/en_gb/united-kingdom/home/car-engine-oil-and-fluids/motor-oil-and-fluids-finder.html" class="nr-navigation__quick-link" target="_self">
                                        <img class="nr-navigation__icon" src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/icons/motor-oil-can-white.svg" alt="icon">
                                        <span class="nr-navigation__link-text">Find the Right Oil</span>
                                    </a>
                                </li>
                            
                                <li class="nr-navigation__list-item">
                                    <a href="https://thelubricantoracle.castrol.com" rel="external" class="nr-navigation__quick-link" target="_blank">
                                        <img class="nr-navigation__icon" src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/icons/industrial-oil-barrel-white.svg" alt="icon">
                                        <span class="nr-navigation__link-text">Industrial Lubricant Finder</span>
                                    </a>
                                </li>
                            
                                <li class="nr-navigation__list-item">
                                    <a href="http://msdspds.castrol.com/msdspds/msdspds.nsf/CastrolSearch?OpenForm&amp;sitelang=EN&amp;output=Full" rel="external" class="nr-navigation__quick-link" target="_blank">
                                        <img class="nr-navigation__icon" src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/icons/product-data-sheets-white.svg" alt="icon">
                                        <span class="nr-navigation__link-text">Product Data Sheets</span>
                                    </a>
                                </li>
                            
                                <li class="nr-navigation__list-item">
                                    <a href="https://maps.castrol.com/?types=26" rel="external" class="nr-navigation__quick-link" target="_blank">
                                        <img class="nr-navigation__icon" src="https://www.castrol.com/content/dam/castrol/master-site/en/global/home/icons/where-to-buy-white.svg" alt="icon">
                                        <span class="nr-navigation__link-text">Find Workshop</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
        </aside>
        ';
    
    ?>

    