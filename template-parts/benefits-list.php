<?php
    
    // $benefits_img = '/inc/img/cas002-benefits-image-1.jpg';
    $benefits_img = get_template_directory_uri() . '/inc/img/cas002-benefits-image-1.jpg';
    $list_dot       = get_template_directory_uri() . '/inc/img/cas002-list-dot.png';

    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">

            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper
                            nr-layout__wrapper--stacked
                            nr-layout__wrapper--central
                            nr-layout__wrapper--middle">
                    
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">

                        <h2>We provide and manage your lubricants and fluids – so you don’t have to</h2>

                    </div>

                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>
        
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper nr-layout__wrapper--sidebyside nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight">
                    
                    <div data-component-name="list" data-component-container="true" class="nr-list-component nr-component aem-GridColumn aem-GridColumn--default--12">
                        
                        <div class="nr-list" data-list-path="/content/castrol/country-sites/en_gb/united-kingdom/home/jcr:content/root/layout_copy/list">

                            <h2 class="nr-list__title">
                                You’ll benefit from:
                            </h2>

                            <div class="nr-list__wrap">
                                
                                <ul class="nr-list__item-wrap js-list-wrap" data-resultmsg="No items found" style="list-style-image: url(' . $benefits_img . ');">
                                    <li class="nr-list__item">
                                        Optimised metalworking fluid, chemical & lubrication usage and management
                                    </li>
                                    <li class="nr-list__item">
                                        Access to specialized application, procurement & inventory control knowledge
                                    </li>
                                    <li class="nr-list__item">
                                        Expert health, safety & environmental initiatives
                                    </li>
                                    <li class="nr-list__item">
                                        Simplified chemical tracking and streamline waste management compliance
                                    </li>
                                    <li class="nr-list__item">
                                        Improved uptime, productivity & safety and supercharge your performance 
                                    </li>
                                    <li class="nr-list__item">
                                        Cost-effective sourcing and realise sustainable cost reductions 
                                    </li>
                        
                                </ul>
                    
                            </div>
                        </div>
                    </div>

                    <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12" style="margin-bottom: 103.462px;">

                        <div class="nr-cards__overlay" style="top: -36.7311px;"></div>
                            
                            <div class="nr-cards__media nr-lazy-size__wrapper">

                                <img data-src="' . $benefits_img . '" data-srcset="' . $benefits_img . ' 1x, ' . $benefits_img . ' 2x" title="Castrol Story" alt="Castrol Story" src="' . $benefits_img . '" width="1024" height="100%" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $benefits_img . ' 1x, ' . $benefits_img . ' 2x">
                            </div>
                        
                    </div>
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">
                        <p class="cq-text-placeholder-ipe" data-emptytext="Rich Text Editor">
                    
                        </p>
                    </div>
                    <div data-component-name="linkcta" data-component-container="true" class="nr-linkcta-component nr-component aem-GridColumn aem-GridColumn--default--12">

                    </div>

                        <div class="nr-layout__component new section aem-Grid-newComponent">

                    </div>
                </div>
            </div>
        </div>
    ';
?>