<?php

    echo '
        <style>
            .section__contact {
                text-align: center;
            }
            .section__contact-title {=
                color: white;
                margin-bottom: 1.5rem;
                font-size: 1.95rem;
                font-family: CastrolSansCon-Regular, sans-serif, sans-serif;
                font-weight: 600;
                text-transform: uppercase;
                text-transform: full-width;
                -webkit-transform: scale(0.7, 1);
                -moz-transform: scale(0.7, 1);
                -ms-transform: scale(0.7, 1);
                -o-transform: scale(0.7, 1);
                transform: scale(0.7, 1);
            }
            .section__contact-copy {
                margin-bottom: 5.415rem;
                font-family: "Arial Narrow", sans-serif;
                font-size: 1.2rem;
                line-height: 1.475rem;
            }
            .section__contact-input {
                width: 70%;
                margin: 0 auto;
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            .section__contact-input label {
                font-weight: bold;
            }
            .section__contact-input-submit {
                position: relative;
            }
            .section__contact-input-message {
                align-items: start;
            }
            .section__contact-input-message label {
                margin-top: 0.25rem;
            }
            .section__contact-input input[type="text"],
            .section__contact-input input[type="email"],
            .section__contact-input textarea {
                background: none;
                color: white;
                width: 50%;
                margin: 0.25rem 0;
                padding: 0.25rem 0.5rem;
                border: 1px solid #cacaca;
                border-radius: 0.15rem;
            }
            .section__contact-input input[type="text"]:focus,
            .section__contact-input input[type="email"]:focus,
            .section__contact-input textarea:focus {
                outline: none;
            }
            input[type="submit"] {
                position: absolute;
                left: 50%;
                background: #ffffff;
                padding: 0.5rem 1.5rem;
                margin-top: 1rem;
                font-size: 1.25rem;
                border: none;
                border-radius: 0.15rem;
                color: #535759;
                transition: 300ms;
            }
            input[type="submit"]:hover {
                background: #535759;
                color: #ffffff;
                padding: 0.5rem 1.5rem;
            }
        </style>

        <div class="section__contact" style="background: #009343; color: #fff; padding-block: 7.25rem; display: flex; flex-direction: column; align-items: center; justify-content: center;">

            <div style="width: 40%;">
        
                <h2 class="section__contact-title">Let’s chat….</h2>

                <p class="section__contact-copy">To discover how we can help your manufacturing business to optimise asset performance, mitigate risk and enhance operational efficiency across all your sites, simply fill in the form below and an expert member of our team will be in touch…</p>

                <form action="">

                    <div class="section__contact-input">
                        <label for="name">Your name:</label>
                        <input type="text" name="name" id="">
                    </div>
                    <div class="section__contact-input">
                        <label for="business-name">Your business name:</label>
                        <input type="text" name="business-name" id="">
                    </div>
                    <div class="section__contact-input">
                        <label for="email">Your email:</label>
                        <input type="email" name="email" id="">
                    </div>
                    <div class="section__contact-input section__contact-input-message">
                        <label for="message">Your message:</label>
                        <textarea name="message" id="" cols="30" rows="5"></textarea>
                    </div>
            
                    <div class="section__contact-input-submit">
                        <input type="submit" value="Submit">
                    </div>
            
                </form>

            </div>
        
        </div>
    ';

?>
