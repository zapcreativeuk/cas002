<?php
    
    // $benefits_img = '/inc/img/cas002-benefits-image-1.jpg';
    $benefits_img = get_template_directory_uri() . '/inc/img/cas002-benefits-image-1.jpg';
    $list_dot       = get_template_directory_uri() . '/inc/img/cas002-list-dot.png';

    $benefits_title       = get_field( 'benefits_title' );
    $benefits_image       = get_field( 'benefits_image' );
    $benefits_list_title  = get_field( 'benefits_list_title' );
    $benefits_list_icon   = get_field( 'benefits_list_icon' );
    $benefits_list        = get_field( 'benefits_list' );

    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">

            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper nr-layout__wrapper--stacked nr-layout__wrapper--central nr-layout__wrapper--middle">
                    
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">

                        <h2 style="text-align: center;">' . $benefits_title . '</h2>

                    </div>

                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>
        
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper nr-layout__wrapper--sidebyside nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight">
                    
                    <div data-component-name="list" data-component-container="true" class="nr-list-component nr-component aem-GridColumn aem-GridColumn--default--12">
                        
                        <div class="nr-list" data-list-path="/content/castrol/country-sites/en_gb/united-kingdom/home/jcr:content/root/layout_copy/list">

                            <h2 class="nr-list__title">
                                ' . $benefits_list_title . ':
                            </h2>

                            <div class="nr-list__wrap">
                                
                                <ul class="nr-list__item-wrap js-list-wrap" data-resultmsg="No items found">';
                                
                                    // Check rows exists.
                                    if( have_rows( 'benefits_list' ) ):

                                        // Loop through rows.
                                        while( have_rows( 'benefits_list' ) ) : the_row();

                                            // Load sub field value.
                                            $benefits_list_item = get_sub_field( 'benefits_list_item' );
                                            
                                            echo '
                                                <li class="nr-list__item">
                                                    <img src="' . $benefits_list_icon['url'] . '" style="height: 54px; width: 54px;" />' . $benefits_list_item . '
                                                </li>
                                            ';

                                        endwhile;
                                    endif;

                                echo '
                        
                                </ul>
                    
                            </div>
                        </div>
                    </div>

                    <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12" style="margin-bottom: 103.462px;">

                        <div class="nr-cards__overlay" style="top: -36.7311px;"></div>
                            
                            <div class="nr-cards__media nr-lazy-size__wrapper">

                                <img class="class="nr-lazy-size nr-cards__media-img lazyloaded" src="' . $benefits_image['url'] . '" style="width: 100%;" >
                                
                            </div>
                        
                    </div>
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">
                        <p class="cq-text-placeholder-ipe" data-emptytext="Rich Text Editor">
                    
                        </p>
                    </div>
                    <div data-component-name="linkcta" data-component-container="true" class="nr-linkcta-component nr-component aem-GridColumn aem-GridColumn--default--12">

                    </div>

                        <div class="nr-layout__component new section aem-Grid-newComponent">

                    </div>
                </div>
            </div>
        </div>
    ';
?>