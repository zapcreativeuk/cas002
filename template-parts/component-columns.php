<?php
    $column_title = get_field( 'three_column_title' );
    $column_copy  = get_field( 'three_column_copy' );
    $column_cards = get_field( 'three_column_cards' );

    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                <div class="nr-layout__wrapper nr-layout__wrapper--stacked nr-layout__wrapper--central nr-layout__wrapper--middle">
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">
                        <h2 style="text-align: center;">' . $column_title . '</h2>
                        <p style="text-align: center;">' . $column_copy . '</p>
                    </div>

                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>

            <!-- start 3 columns -->

            <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
                <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                    <div class="nr-layout__wrapper nr-layout__wrapper--row3 nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight">';

                        if( have_rows('three_column_cards') ):
                            while( have_rows('three_column_cards') ) : the_row();

                                $three_column_card_title    = get_sub_field('three_column_card_title');
                                $three_column_card_copy     = get_sub_field('three_column_card_copy');
                                $three_column_card_image    = get_sub_field('three_column_card_image');
                                $three_column_card_link     = get_sub_field('three_column_card_link');

                                echo '
                                    <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12" style="margin-bottom: 103.462px;">
                                        <a class="nr-cards  nr-cards__no-description" href="' . $three_column_card_link . '" target="_self">
                                            <div class="nr-cards__overlay" style="top: -36.7311px;"></div>
                                            <div class="nr-cards__media nr-lazy-size__wrapper">
                                                <img class="" src="' . $three_column_card_image['url'] . '" style="width: 100%;" >
                                                
                                            </div>
                                            <div class="nr-cards__content">
                                                <h2 class="nr-cards__head">
                                                    ' . $three_column_card_title . '
                                                </h2>
                                                <div class="nr-cards__description">
                                                    <p class="nr-cards__description-text" style="text-align: center;">' . $three_column_card_copy . '</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                ';
                            endwhile;
                        endif;
                        
                        wp_reset_postdata();

                echo 
                    '</div>
                </div>
            </div>
        </div>
    </div>
    ';
    
    wp_reset_postdata();
?>