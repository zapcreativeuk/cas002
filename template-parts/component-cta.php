<?php

    $cta_title          = get_field( 'cta_title' );
    $cta_copy           = get_field( 'cta_copy' );
    $cta_contact_form   = get_field( 'cta_contact_form' );

    echo '
        <style>
            .section__contact {
                text-align: center;
            }
            .section__contact__container {
                width: 100%;
                padding: 0.625rem;
            }
            @media(min-width: 768px) {
                .section__contact__container {
                    width: 90%;
                }
            }
            @media(min-width: 992px) {
                .section__contact__container {
                    width: 80%;
                }
            }
            @media(min-width: 1200px) {
                .section__contact__container {
                    width: 50%;
                }
            }
            .section__contact-title {
                color: white;
                margin-bott6m: 1.5rem;
                font-size: 1.95rem;
                font-family: CastrolSansCon-Regular, sans-serif, sans-serif;
                font-weight: 600;
                text-transform: uppercase;
            }
            .section__contact-copy {
                margin-bottom: 5.415rem;
                font-family: "Arial Narrow", sans-serif;
                font-size: 1.2rem;
                line-height: 1.475rem;
            }
            .section__contact-input {
                width: 100%;
                margin: 0 auto;
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            @media(min-width: 768px) {
                .section__contact-input {
                    width: 90%;
                }
            }
            @media(min-width: 992px) {
                .section__contact-input {
                    width: 80%;
                }
            }
            @media(min-width: 1200px) {
                .section__contact-input {
                    width: 70%;
                }
            }
            .wpcf7-form-control-wrap {
                width: 50%;
            }
            .section__contact-input label {
                width: 100%;
                font-weight: bold;
                display: flex;
                justify-content: space-between;
            }
            .wpcf7-form-control {
            }
            .section__contact-input-submit {
                position: relative;
            }
            .section__contact-input-message {
                align-items: start;
            }
            .section__contact-input-message label {
                margin-top: 0.25rem;
            }
            .section__contact-input input[type="text"],
            .section__contact-input input[type="email"],
            .section__contact-input textarea {
                background: none;
                color: white;
                width: 100%;
                margin: 0.25rem 0;
                padding: 0.25rem 0.5rem;
                border: 1px solid #cacaca;
                border-radius: 0.15rem;
            }
            .section__contact-input input[type="text"]:focus,
            .section__contact-input input[type="email"]:focus,
            .section__contact-input textarea:focus {
                outline: none;
            }
            input[type="submit"] {
                position: absolute;
                left: 50%;
                background: #ffffff;
                padding: 0.5rem 1.5rem;
                margin-top: 1rem;
                font-size: 1.25rem;
                border: none;
                border-radius: 0.15rem;
                color: #535759;
                transition: 300ms;
            }
            input[type="submit"]:hover {
                background: #535759;
                color: #ffffff;
                padding: 0.5rem 1.5rem;
            }
        </style>

        <div class="section__contact" style="background: #009343; color: #fff; padding-block: 7.25rem; display: flex; flex-direction: column; align-items: center; justify-content: center;">

            <div class="section__contact__container">
        
                <h2 class="section__contact-title">' . $cta_title . '</h2>

                <p class="section__contact-copy">' . $cta_copy . '</p>

                <div>
                    ' . $cta_contact_form . '
                </div>

            </div>
        
        </div>
    ';

?>

<!-- <form action="">

    <div class="section__contact-input">
        <label for="name">Your name:</label>
        <input type="text" name="name" id="">
    </div>
    <div class="section__contact-input">
        <label for="business-name">Your business name:</label>
        <input type="text" name="business-name" id="">
    </div>
    <div class="section__contact-input">
        <label for="email">Your email:</label>
        <input type="email" name="email" id="">
    </div>
    <div class="section__contact-input section__contact-input-message">
        <label for="message">Your message:</label>
        <textarea name="message" id="" cols="30" rows="5"></textarea>
    </div>

    <div class="section__contact-input-submit">
        <input type="submit" value="Submit">
    </div>

</form> -->