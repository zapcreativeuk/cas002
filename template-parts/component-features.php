<?php 
    $features_centre_image = get_field( 'features_centre_image' );

    echo '
        <div data-component-name="features" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12 component-features">
            

            <div data-component-name="features-row-top" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
                
                <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                    
                    <div class="nr-layout__wrapper nr-layout__wrapper--row3 nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight">';

                        if( have_rows('features_top_row') ):
                            while( have_rows('features_top_row') ) : the_row();

                                $top_row_card_title = get_sub_field('top_row_card_title');
                                $top_row_card_copy  = get_sub_field('top_row_card_copy');

                                echo '
                                    <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12">
                                
                                        <div class="nr-cards__overlay" style="top: -36.7311px;">
                                        </div>
                                        <div class="nr-cards__content">
                                            <h3 class="nr-cards__head">' . $top_row_card_title . '</h3>
                                            <div class="nr-cards__description">
                                                <p class="nr-cards__description-text">' . $top_row_card_copy . '</p>
                                            </div>
                                        </div>
                                    </div>
                                ';

                            endwhile;
                        endif;

                        wp_reset_postdata();

                        echo '
                            <div class="nr-layout__component new section aem-Grid-newComponent">
                            </div>    

                        </div>
                    </div>
                </div>
            </div>

                <div data-component-name="feature-image" data-component-container="true" class="nr-carousel-component nr-component aem-GridColumn aem-GridColumn--default--12">
                    <section class="nr-feature-carousel nr-feature-carousel--dark">
                        <img data-src="' . $features_centre_image['url'] . '" data-srcset="' . $features_centre_image['url'] . ' 1x, ' . $features_centre_image['url'] . '" src="' . $features_centre_image['url'] . '"width="1440" height="100%" class="nr-feature-carousel__media nr-lazy-size lazyloaded" srcset="' . $features_centre_image['url'] . ' 1x, ' . $features_centre_image['url'] . '">
                    </section>
                </div>

            <div data-component-name="features-row-bottom" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
                
                <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                    
                    <div class="nr-layout__wrapper nr-layout__wrapper--row3 nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight">';

                        if( have_rows('features_bottom_row') ):
                            while( have_rows('features_bottom_row') ) : the_row();

                                // Load sub field value.
                                $bottom_row_card_title = get_sub_field( 'bottom_row_card_title' );
                                $bottom_row_card_copy  = get_sub_field( 'bottom_row_card_copy' );

                                echo '
                                    <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12">
                                
                                        <div class="nr-cards__overlay" style="top: -36.7311px;">
                                        </div>
                                        <div class="nr-cards__content">
                                            <h3 class="nr-cards__head">' . $bottom_row_card_title . '</h3>
                                            <div class="nr-cards__description">
                                                <p class="nr-cards__description-text">' . $bottom_row_card_copy . '</p>
                                            </div>
                                        </div>
                                    </div>
                                ';

                            endwhile;
                        endif;

                        wp_reset_postdata();
                    
                    echo '
                            <div class="nr-layout__component new section aem-Grid-newComponent">

                            </div>    

                        </div>
                    </div>
                </div>
            </div>

        </div>
    ';

?>