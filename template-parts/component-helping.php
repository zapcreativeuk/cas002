<?php    
    $helping_title = get_field( 'helping_title' );

    echo '

        <style>
            .nr-component-grey {
                background: #ededed;
                padding-block: 6.375rem;
            }
            
            .nr-component-grey > .nr-layout-component {
                background: none;
            }
            
            .nr-component-grey > .nr-cards__media-img {
                max-width: 100%;
                margin: 0 auto;
            }
            
        </style>

        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component nr-component-grey aem-GridColumn aem-GridColumn--default--12">
        
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper nr-layout__wrapper--stacked nr-layout__wrapper--central nr-layout__wrapper--middle">
                    
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">

                        <h2 style="font-size: 1.95rem;
                        text-align: center;">' . $helping_title . '</h2>

                    </div>

                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>

            <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
                    
                    <div class="nr-layout__wrapper nr-layout__wrapper--row3 nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight" style="justify-content: space-between; align-items: start;">';
                        
                        if( have_rows( 'helping_cards' ) ):
                            while( have_rows( 'helping_cards' ) ) : the_row();

                                // Load sub field value.
                                $helping_card_title = get_sub_field( 'helping_card_title' );
                                $helping_card_icon  = get_sub_field( 'helping_card_icon' );
                                $helping_card_copy  = get_sub_field( 'helping_card_copy' );

                                echo '
                                    <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12"  style="margin: 0; display: flex; justify-content: center; flex-direction: column; align-items: center;">
                                        
                                        <div class="nr-cards__media nr-lazy-size__wrapper" style="display: flex; flex-direction: column; align-items: center; height: 150px; width: 150px; justify-content: center;">
                                            <img data-src="' . $helping_card_icon['url'] . '" data-srcset="' . $helping_card_icon['url'] . ' 1x, ' . $helping_card_icon['url'] . ' 2x" title="Car Engine Oil &amp; Fluids" alt="Car Engine Oil &amp; Fluids" src="' . $helping_card_icon['url'] . '" width="115px" height="118px" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $helping_card_icon['url'] . ' 1x, ' . $helping_card_icon['url'] . ' 2x" style="height: ' . $helping_card_icon['height'] . 'px !important; width: ' . $helping_card_icon['width'] . 'px;">
                                        </div>
                                        <div class="nr-cards__content">
                                            <h3 class="nr-cards__head">
                                                ' . $helping_card_title . '
                                            </h3>
                                            <div class="nr-cards__description">
                                                <p class="nr-cards__description-text" style="text-align: center;">' . $helping_card_copy . '</p>
                                            </div>
                                        </div>
            
                                    </div>
                                ';

                            endwhile;
                        endif;

                        wp_reset_postdata();

                    echo '
                        <div class="nr-layout__component new section aem-Grid-newComponent"></div>
                    </div>
                </div>
            </div>
        </div>
    ';
?>

</section>