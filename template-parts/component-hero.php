<?php
    // Hero Image
    $hero_title = get_field( 'hero_title' );
    $hero_image = get_field( 'hero_image' );

    echo '
        <style>
            .hero-title {
                color: rgba(255,255,255,0.7); 
                margin: 0; 
                font-size: 2rem;
                font-weight: 400;
            }

            @media(min-width: 768px) {
                .hero-title {
                    font-size: 2.4rem;
                }
            }
            @media(min-width: 992px) {
                .hero-title {
                    font-size: 3rem
                }
            }
            @media(min-width: 1200px) {
                .hero-title {
                    font-size: 6rem
                }
            }
        </style>

        <div data-component-name="featureCarousel" data-component-container="true" class="nr-carousel-component nr-component aem-GridColumn aem-GridColumn--default--12">
            <section class="nr-feature-carousel nr-feature-carousel--dark">
                <img data-src="' . $hero_image['url'] . '" data-srcset="' . $hero_image['url'] . ' 1x, ' . $hero_image['url'] . '" src="' . $hero_image['url'] . '"width="1440" height="100%" class="nr-feature-carousel__media nr-lazy-size lazyloaded" srcset="' . $hero_image['url'] . ' 1x, ' . $hero_image['url'] . '">
                <div class="nr-feature-carousel__overlay" style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; padding: 2rem 4rem;">
                    <h1 class="hero-title">' . $hero_title . '</h1>
                </div>
            </section>
        </div>
    ';
?>