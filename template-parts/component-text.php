<?php

    $text_title = get_sub_field( 'text_title' );
    $text_copy  = get_sub_field( 'text_copy'  );

    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                <div class="nr-layout__wrapper nr-layout__wrapper--stacked nr-layout__wrapper--central nr-layout__wrapper--middle">
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">
                        <h2 style="text-align: center;">' . $text_title . '</h2>
                        <p style="text-align: center;">' . $text_copy . '</p>
                    </div>
                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>
        </div>
    ';
?>