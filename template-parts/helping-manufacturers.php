<?php
    $col3_img1  = get_template_directory_uri() . '/inc/img/CAS002_Complete_CMS-consuption.png';
    $col3_img2  = get_template_directory_uri() . '/inc/img/CAS002_Complete_CMS-waste.png';
    $col3_img3  = get_template_directory_uri() . '/inc/img/CAS002_Complete_CMS-failures.png';

    echo '

        <style>
            .nr-component-grey {
                background: #ededed;
                padding-block: 6.375rem;
            }
            
            .nr-component-grey > .nr-layout-component {
                background: none;
            }
            
            .nr-component-grey > .nr-cards__media-img {
                max-width: 100%;
                margin: 0 auto;
            }
            
        </style>

        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component nr-component-grey aem-GridColumn aem-GridColumn--default--12">
        
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper nr-layout__wrapper--stacked nr-layout__wrapper--central nr-layout__wrapper--middle">
                    
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">

                        <h2 style="font-size: 1.95rem;
                        text-align: center;">Castrol Complete<sup>CMS</sup> IS HELPING<br>MANUFACTURERS TO…</h2>
                        <p style="text-align: center;">Castrol CompleteCMS brings real value to your business by taking total ownership of your chemical and fluid operation – from procurement and stock control to optimising system performance – all with the simple objective of driving costs savings and continuous improvements at your business, year after year.</p>

                    </div>

                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>

            <!-- start 3 columns -->

            <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
            
                <!-- start column 1 -->
                    
                    <div class="nr-layout__wrapper nr-layout__wrapper--row3 nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight" style="justify-content: space-between; align-items: start;">
                        
                        <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12"  style="margin: 0; display: flex; justify-content: center; flex-direction: column; align-items: center;">
                            
                            <div class="nr-cards__media nr-lazy-size__wrapper" style="display: flex; flex-direction: column; align-items: center; height: 150px; width: 150px; justify-content: center;">
                                <img data-src="' . $col3_img1 . '" data-srcset="' . $col3_img1 . ' 1x, ' . $col3_img1 . ' 2x" title="Car Engine Oil &amp; Fluids" alt="Car Engine Oil &amp; Fluids" src="' . $col3_img1 . '" width="115px" height="118px" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $col3_img1 . ' 1x, ' . $col3_img1 . ' 2x" style="height: 118px !important; width: 115px;">
                            </div>
                            <div class="nr-cards__content">
                                <h3 class="nr-cards__head">
                                    Reduce consumption
                                </h3>
                                <div class="nr-cards__description">
                                    <p class="nr-cards__description-text" style="text-align: center;">We helped an automotive engine manufacturer reduce their particulate PPM by more than 10x, and introduced a filtration system which is saving the company £70,000 per year</p>
                                </div>
                            </div>

                        </div>
                        <!-- start column 2 -->
                        <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12"  style="margin: 0; display: flex; justify-content: center; flex-direction: column; align-items: center;">
                            
                            <div class="nr-cards__media nr-lazy-size__wrapper" style="display: flex; flex-direction: column; align-items: center; height: 150px; width: 150px; justify-content: center;">
                                <img data-src="' . $col3_img2 . '" data-srcset="' . $col3_img2 . ' 1x, ' . $col3_img2 . ' 2x" title="Motorcycle Oil and Fluids" alt="Motorcycle Oil and Fluids" src="' . $col3_img2 . '" width="74px" height="136px" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $col3_img2 . ' 1x, ' . $col3_img2 . ' 2x" style="width: 74px; height: 136px;">
                            </div>
                            <div class="nr-cards__content">
                                <h3 class="nr-cards__head">
                                    Minimise waste
                                </h3>
                                <div class="nr-cards__description">
                                    <p class="nr-cards__description-text" style="text-align: center;">We helped an automotive engine manufacturer to achieve a significant reduction in wastewater and extend machine tool life, resulting in a financial benefit of over £216,000</p>
                                </div>
                            </div>

                        </div>
                        <!-- start column 3 -->
                        <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12"  style="margin: 0; display: flex; justify-content: center; flex-direction: column; align-items: center;">
                            
                            <div class="nr-cards__media nr-lazy-size__wrapper" style="display: flex; flex-direction: column; align-items: center; height: 150px; width: 150px; justify-content: center;">
                                <img data-src="' . $col3_img3 . '" data-srcset="' . $col3_img3 . ' 1x, ' . $col3_img3 . ' 2x" title="Commercial Vehicle Oil and Fluids" alt="Commercial Vehicle Oil and Fluids" src="' . $col3_img3 . '" width="128px" height="117px" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $col3_img3 . ' 1x, ' . $col3_img3 . ' 2x" style="width: 128px; height: 117px;">
                            </div>
                            <div class="nr-cards__content">
                                <h3 class="nr-cards__head">
                                    Achieve zero gearbox failures
                                </h3>
                                <div class="nr-cards__description">
                                    <p class="nr-cards__description-text" style="text-align: center;">We helped a steel manufacturer to achieve zero gearbox failures for 1 year, with an estimated financial benefit of £699,000</p>
                                </div>
                            </div>
                        
                        </div>

                        <div class="nr-layout__component new section aem-Grid-newComponent"></div>    

                        </div>
                    </div>
                <!-- </div> -->
            </div>
        </div>
    ';
?>

</section>