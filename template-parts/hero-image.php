<?php
    // Hero Image
    $page_title = get_the_title();
    $hero_image = get_template_directory_uri() . '/inc/img/cas002-hero-image.jpg';

    echo '
        <div data-component-name="featureCarousel" data-component-container="true" class="nr-carousel-component nr-component aem-GridColumn aem-GridColumn--default--12">
            <section class="nr-feature-carousel nr-feature-carousel--dark">
                <img data-src="' . $hero_image . '" data-srcset="' . $hero_image . ' 1x, ' . $hero_image . '" src="' . $hero_image . '"width="1440" height="100%" class="nr-feature-carousel__media nr-lazy-size lazyloaded" srcset="' . $hero_image . ' 1x, ' . $hero_image . '">
                <div class="nr-feature-carousel__overlay" style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; padding: 2rem 4rem;">
                    <h1 style="font-size: 5rem; margin: 0; color: rgba(255,255,255,0.7); font-weight: 400;">' . $page_title . '</h1>
                </div>
            </section>
        </div>
    ';
?>