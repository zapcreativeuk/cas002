<?php
    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                <div class="nr-layout__wrapper nr-layout__wrapper--stacked nr-layout__wrapper--central nr-layout__wrapper--middle">
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">
                        <h2>When you purchase Chemical Management Services, what do you expect from your supplier?</h2>
                        <p>You likely believe they should supply and manage all of the chemicals you use at your manufacturing plant. At Castrol, we agree – but when you work with us, we want you to expect a whole lot more. That’s why we’re re-defining CMS… </p>
                    </div>
                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>
        </div>
    ';
?>