<?php
    
    // $benefits_img = '/inc/img/cas002-benefits-image-1.jpg';
    $benefits_img = get_template_directory_uri() . '/inc/img/cas002-benefits-image-1.jpg';
    $list_dot       = get_template_directory_uri() . '/inc/img/cas002-list-dot.png';

    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12" style="display: flex; justify-content: center; align-items: center; height: 11.5625rem;">

            <div class="pre-footer-logo" style="width: 180px; height: auto;">
                <img class="nr-navigation__logo-img" width="180px" height="auto" src="https://www.castrol.com/apps/settings/wcm/designs/refresh/castrol/images/castrol.svg" alt="Castrol Logo">    
            </div>
        
        </div>
    ';
?>