<?php
    $col3_img1  = get_template_directory_uri() . '/inc/img/cas002-3col-image-1.jpg';
    $col3_img2  = get_template_directory_uri() . '/inc/img/cas002-3col-image-2.jpg';
    $col3_img3  = get_template_directory_uri() . '/inc/img/cas002-3col-image-3.jpg';

    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
        
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper
                            nr-layout__wrapper--stacked
                            nr-layout__wrapper--central
                            nr-layout__wrapper--middle">
                    
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">

                        <h2>Castrol Complete<sup>CMS<sup></h2>
                        <p>Castrol CompleteCMS brings real value to your business by taking total ownership of your chemical and fluid operation – from procurement and stock control to optimising system performance – all with the simple objective of driving costs savings and continuous improvements at your business, year after year.</p>

                    </div>

                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>

            ' . /* start 3 columns  */  '

            <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
            
                ' . /* start column 1  */  '
                <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                    
                    <div class="nr-layout__wrapper nr-layout__wrapper--row3 nr-layout__wrapper--full nr-layout__wrapper--top nr-layout__equalHeight">
                        
                        <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12" style="margin-bottom: 103.462px;">
                
                            <a class="nr-cards  nr-cards__no-description" href="/en_gb/united-kingdom/home/car-engine-oil-and-fluids.html" target="_self">
                                <div class="nr-cards__overlay" style="top: -36.7311px;"></div>
                                
                                <div class="nr-cards__media nr-lazy-size__wrapper">
                                    <img data-src="' . $col3_img1 . '" data-srcset="' . $col3_img1 . ' 1x, ' . $col3_img1 . ' 2x" title="Car Engine Oil &amp; Fluids" alt="Car Engine Oil &amp; Fluids" src="' . $col3_img1 . '" width="500" height="100%" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $col3_img1 . ' 1x, ' . $col3_img1 . ' 2x">
                                </div>
                                <div class="nr-cards__content">
                                    <h3 class="nr-cards__head">
                                        Connect
                                    </h3>
                                    <div class="nr-cards__description">
                                        <p class="nr-cards__description-text">Revolutionise your approach with our innovative range of smart technologies, which will enable you to continuously improve asset optimisation, risk mitigation and supply chain effectiveness.</p>
                                    </div>
                                </div>
                            </a>

                            </div>
                            ' . /* start column 2  */  '
                            <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12" style="margin-bottom: 103.462px;">
                            
                            
                                <a class="nr-cards  nr-cards__no-description" href="/en_gb/united-kingdom/home/motorcycle-oil-and-fluids.html" target="_self">
                                    <div class="nr-cards__overlay" style="top: -36.7311px;"></div>
                                    
                                    <div class="nr-cards__media nr-lazy-size__wrapper">
                                        <img data-src="' . $col3_img2 . '" data-srcset="' . $col3_img2 . ' 1x, ' . $col3_img2 . ' 2x" title="Motorcycle Oil and Fluids" alt="Motorcycle Oil and Fluids" src="' . $col3_img2 . '" width="500" height="100%" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $col3_img2 . ' 1x, ' . $col3_img2 . ' 2x">
                                    </div>
                                    <div class="nr-cards__content">
                                        <h3 class="nr-cards__head">
                                            Maintain
                                        </h3>
                                        <div class="nr-cards__description">
                                            <p class="nr-cards__description-text">Optimise costs, minimise downtime and waste, and maximise productivity with our industry-leading support, expertise and innovative solutions. </p>
                                        </div>
                                    </div>
                                </a>
                            

                            </div>
                            ' . /* start column 3  */  '
                            <div data-component-name="cards" data-component-container="true" class="nr-cards-component nr-component aem-GridColumn aem-GridColumn--default--12" style="margin-bottom: 103.462px;">
                                
                                
                                <a class="nr-cards  nr-cards__no-description" href="/en_gb/united-kingdom/home/heavy-commercial-vehicles-oil-and-fluids.html" target="_self">
                                    <div class="nr-cards__overlay" style="top: -36.7311px;"></div>
                                    
                                    <div class="nr-cards__media nr-lazy-size__wrapper">
                                        <img data-src="' . $col3_img3 . '" data-srcset="' . $col3_img3 . ' 1x, ' . $col3_img3 . ' 2x" title="Commercial Vehicle Oil and Fluids" alt="Commercial Vehicle Oil and Fluids" src="' . $col3_img3 . '" width="500" height="100%" class="nr-lazy-size nr-cards__media-img lazyloaded" srcset="' . $col3_img3 . ' 1x, ' . $col3_img3 . ' 2x">
                                    </div>
                                    <div class="nr-cards__content">
                                        <h3 class="nr-cards__head">
                                            Succeed
                                        </h3>
                                        <div class="nr-cards__description">
                                            <p class="nr-cards__description-text">With our systematic approach to driving operational efficiencies, you could realise continuous and sustainable improvements across your output and revenue, and minimise your total cost of ownership.</p>
                                        </div>
                                    </div>
                                </a>
                                
                            </div>

                            <div class="nr-layout__component new section aem-Grid-newComponent">

                            </div>    

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ';
?>