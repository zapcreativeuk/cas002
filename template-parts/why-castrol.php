<?php

    // Opening Text
    echo '
        <div data-component-name="layout" data-component-container="true" class="nr-layout-component nr-component aem-GridColumn aem-GridColumn--default--12">
        
            <div class="nr-layout aem-Grid aem-Grid--12 aem-Grid--default--12  ">
                
                <div class="nr-layout__wrapper
                            nr-layout__wrapper--stacked
                            nr-layout__wrapper--central
                            nr-layout__wrapper--middle">
                    
                    <div data-component-name="RTE" data-component-container="true" class="nr-text-component nr-component aem-GridColumn aem-GridColumn--default--12">

                        <h2>Why Castrol?</h2>
                        <p>Castrol aims to consistently deliver improved productivity and cost savings to our customers around the world – giving them the performance advantage they expect from a leader. One hundred years of world leadership in metalworking fluids and lubricants provides Castrol with extensive applications expertise to bring you the advantage. A comprehensive range of best-in-class products and services, enhanced by unique technology platforms and enablers, allows our customers to sustain this advantage.</p>

                    </div>

                    <div class="nr-layout__component new section aem-Grid-newComponent">
                    </div>
                </div>
            </div>
        </div>    
    ';

?>